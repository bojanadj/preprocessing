# Preprocessing pipeline for Serbian



## What it does

1. Converts all text into latin script
2. Converts it into lower case
3. Tokenizes and lemmatizes all words
3. Converts some (i)jekavian lemmas into ekavian
4. Removes numbers
5. Removes stop words
