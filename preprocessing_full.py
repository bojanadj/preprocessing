# Full pipeline za predobradu tvitova
# Originalni kod je kod Bojane u jupyter notebooks - Nova obrada tvitova.ipynb
# coding: utf-8

get_ipython().system('pip install classla')
get_ipython().system('pip install srtools')
get_ipython().system('pip install conllu')
get_ipython().system('pip install re')
get_ipython().system('pip install many_stop_words ')

import classla
from srtools import cyrillic_to_latin
from conllu import parse
import re
import pandas as pd
import csv
from many_stop_words import get_stop_words
from nltk.tokenize import RegexpTokenizer
from nltk.tokenize import word_tokenize
from csv import writer
from csv import reader

# Inicijalizacija classla pipelinea za tokenizaciju, pos tagovanje i lematizaciju na nestandardnom tekstu
#classla.download('sr', type='nonstandard', processors='tokenize,pos,lemma')
nlp = classla.Pipeline('sr', type='nonstandard', processors='tokenize,pos,lemma')

# Prebacivanje ćirilice u latinicu
# Prebacivanje u mala slova
def preprocess(sentence): 
    text_latin = cyrillic_to_latin(sentence)
    latin_lower = text_latin.lower()
    return latin_lower

# Zamena tekstualnih reči covid i kovid lemom covid (da ne bi lematizirao kao čovjek)
# Zamena reči koje lematizira u jekavicu ekavicom
def lem(sentence):
    hrv_srp = {'dijeliti': 'deliti', 'cel': 'ceo', 'vidjeti': 'videti', 'što': 'šta', 'čovjek': 'čovek', 'vjerovati': 'verovati', 'vjerovatno': 'verovatno', 'tko': 'ko', 'nitko': 'niko', 'netko': 'neko', 'itko': 'iko', 'voljeti': 'voleti', 'željeti': 'želeti', 'umrijeti': 'umreti', 'minuta': 'minut', 'živjeti': 'živeti'}
    sent = []
    zamena = r'[kKcC]ovid|COVID-?\d*' 
    doc = nlp(sentence)
    process = doc.to_conll()
    sentence = parse(process)
    for token in sentence:
        for i in range(len(token)):
            if re.match(zamena, token[i]['form']):
                token[i]['lemma'] = 'covid'
            if token[i]['lemma'] in hrv_srp.keys():
                token[i]['lemma'] = hrv_srp[token[i]['lemma']]
            sent.append(token[i]['lemma'])
    final = ' '.join(sent)
    return final
  
# Brisanje brojeva
def del_numbers(sentence):
    sentence=re.sub(r" ?\d+[.,]", "",sentence) # redni brojevi
    sentence=re.sub("( ?\d+[,.]\d+) din", "", sentence) # brojevi sa tackom izmedju 20.000 i din na kraju
    sentence=re.sub(r"( ?\d+(?:\s+\d+)*) din", "",sentence) #brojevi i brojevi sa razmakom (npr. 20 000) i din na kraju
    sentence=re.sub("[£$€] \d+[,.]\d+", "", sentence)# brojevi sa tackom izmedju 20.000 i £, $ ili € na pocetku
    sentence=re.sub(r" [£$€] (\d+(?:\s+\d+)*)", "",sentence) # brojevi i brojevi sa razmakom (npr. 20 000) i i £, $ ili € na pocetku 
    sentence=re.sub(" ?\d+[,.]\d+", "", sentence) #  brojevi sa tackom izmedju 20.000
    sentence=re.sub(" ?(\d+[/|-]\d+[/|-]\d+)", "",sentence) # Datumi
    sentence=re.sub(r" ?(\d+(?:\s+\d+)*)", "",sentence) # brojevi sa razmakom (npr. 20 000)
    return sentence


# Filtriranje stop reči
# Da bi kod radio, dodati stop reči kao stopwords-sr.txt u folder gde je instaliran paket many_stop_words. 
# Kod mene je ovde: "C:\Users\Bojana\AppData\Local\Programs\Python\Python38\Lib\site-packages\many_stop_words"
# Fajl sa stop rečima je na githubu.

def stop(sentence):    
    stop_words = get_stop_words('sr')
    reg_tokenizer = RegexpTokenizer(r'\w+')
    tokens = reg_tokenizer.tokenize(str(sentence))
    filtered_words = [word for word in tokens if not word in stop_words]   
    sentence = ' '.join(filtered_words)
    return sentence


with open(r"D:\Posao\IVI\Radovi\Sentiment\Veliki USAID rad\negativni_filtered_ispravljenID.csv", 'r', encoding='UTF8') as read_obj, \
    open(r"D:\Posao\IVI\Radovi\Sentiment\Veliki USAID rad\negativni_filtered_ispravljenID_novi_bezstop.csv", 'w', newline='', encoding='UTF8') as write_obj: 

    csv_reader = csv.reader(read_obj)
    next(csv_reader, None) # za preskakanje hedera
    
    csv_writer = writer(write_obj)
    
    for row in csv_reader:
        tweet = row[1]
        tweet_lem = lem(tweet)
        tweet_preprocess = preprocess(tweet_lem)
        tweet_num = del_numbers(tweet_preprocess)
        tweet_stop = stop(tweet_num)
        row.append(tweet_stop)
        csv_writer.writerow(row)    
